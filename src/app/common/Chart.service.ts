import { InjectionToken } from '@angular/core';

export let CHART_TOKEN = new InjectionToken<Object>('Chart');
