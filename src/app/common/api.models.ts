export interface IWelcomeMessage {
    data: IData;
    fact: string;
}

export interface IData {
    m: number;
    km?: number;
    recorder: string;
}
